$(document).ready(function () {

    const button = $('a');
    const blur = $('#blur');
    const popup = $('#popup');
    const body = $('body');

    $(button).click(function (e) {
        e.preventDefault();
        setTimeout(() => {
            $(blur).toggleClass('active');
            $(popup).toggleClass('active');
            if ($(blur).hasClass('active')) {
                $(body).css('overflow', 'hidden');
            } else {
                $(body).css('overflow', 'scroll');
            }
        }, 1);
    });

    $(body).click(function (e) {
        if ($(blur).hasClass('active')) {
            $(blur).toggleClass('active');
            $(popup).toggleClass('active');
            $(body).css('overflow', 'scroll');
        };
    });

    $(popup).click(function (e) {
        if ($(blur).hasClass('active') && $(popup).hasClass('active')) {
            e.stopPropagation();
        };
    });

});